//! A module containing fixed size array iterator like functions
//!
//! The goal of this module is to leverage the size of the array known at compile time, to get more
//! straightforward methods and guaranties at compile time than from an iterator.
//!
//! For example, for an array of size 2, there is always a maximum value, so no need to return an
//! `Option` that whould require `unwrap`.
//!
//! Currently, to get the maximum value of a array, we have to use the iterator method:
//!
//! ```
//! assert_eq!([2, 4, 1, 6, 5].iter().max().unwrap(), &6);
//! ```
//!
//! The ideal way of getting the maximum value of the array would look like:
//!
//! ```compile_fail
//! assert_eq!([2, 4, 1, 6, 5].array_max(), &6);
//! ```
//!
//! _Note: The name `array_max` is used to avoid conflit with already defined `array::max` method._
//!
//! Since it is not possible to define new methods on foreign type to implement this `array_max`
//! method, this module wraps the built-in array type in an `Array` type.
//!
//! Here is an example for the `array_max` method implemented on the `Array` type.
//!
//! ```
//! #![allow(incomplete_features)]
//! #![feature(generic_const_exprs)]
//!
//! use rust_suggestions::array::Array;
//!
//! assert_eq!(Array::from([2, 4, 1, 6, 5]).array_max(), &6);
//! ```
//!
//! Such a `array_max` method would panic on the empty array. This is why the `array_max` method
//! is only defined for array sizes stricly greater than 0.
//!
//!
//! ```compile_fail
//! #![allow(incomplete_features)]
//! #![feature(generic_const_exprs)]
//!
//! use rust_suggestions::array::Array;
//!
//! Array::from([]).max();
//! ```
//!
//! See the `Array` type documentation to see all the methods available.

use crate::const_expr_utils::{Assert, IsTrue};

use core::array;
use core::intrinsics::transmute_unchecked;
use core::mem::MaybeUninit;
use core::ops::{Add, Mul};

/// A built-in array wrapper with custom methods.
///
/// # Conversions from and to built-in array
///
/// An `Array` can be converted from and to a build-in array.
///
/// ```
/// #![allow(incomplete_features)]
/// #![feature(generic_const_exprs)]
/// #![feature(generic_arg_infer)]
///
/// use rust_suggestions::array::Array;
///
/// let array_from = Array::from([1, 2, 3, 4]);
/// let _array_to: [_; _] = array_from.into();
/// ```
///
/// # Generic types
///
/// The `Array` declares generic types and constants:
///
/// - `T`: The array element type, as in the built-in array
/// - `N`: The array size, as in the built-in array
/// - `S`: The start offset, defaulting to `0`, that allows to hide the `S` first elements of the
/// array
/// - `E`: The end offset, defaulting to `0`, that allows to hide the `E` last elements of the array
pub struct Array<T, const N: usize, const S: usize = 0, const E: usize = 0> {
    array: [T; N],
}

/// Implementation of `From` for `Array`.
impl<T, const N: usize> From<[T; N]> for Array<T, N> {
    fn from(value: [T; N]) -> Self {
        Self { array: value }
    }
}

/// Implementation of `Into` for `Array`.
impl<T, const N: usize, const S: usize, const E: usize> Into<[T; N - S - E]> for Array<T, N, S, E>
where
    Assert<{ N >= S + E }>: IsTrue,
{
    fn into(self) -> [T; N - S - E] {
        let mut iter = self.into_iter();
        array::from_fn(|_| iter.next().unwrap())
    }
}

/// Implementation of `Clone` for `Array`.
impl<T: Clone, const N: usize, const S: usize, const E: usize> Clone for Array<T, N, S, E>
where
    Assert<{ N >= S + E }>: IsTrue,
{
    fn clone(&self) -> Self {
        Self {
            array: self.array.clone(),
        }
    }
}

/// Implementation of `Array` for all array sizes.
impl<T, const N: usize, const S: usize, const E: usize> Array<T, N, S, E>
where
    Assert<{ N >= S + E }>: IsTrue,
{
    /// Return the visible size of the array.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([1, 2, 3, 4]);
    /// assert_eq!(array.len(), 4);
    /// ```
    pub const fn len(&self) -> usize {
        N - S - E
    }

    /// Return an iterator over the array's visible elements.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([1, 2, 3, 4]);
    /// for value in array.iter() {
    ///     println!("{}", value);
    /// }
    /// assert_eq!(array.len(), 4);
    /// ```
    pub fn iter(&self) -> impl ExactSizeIterator<Item = &T> {
        self.array[S..(N - E)].iter()
    }

    /// Return an iterator over the array's visible elements, consuming the array.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([1, 2, 3, 4]);
    /// for value in array.into_iter() {
    ///     println!("{}", value);
    /// }
    /// ```
    pub fn into_iter(self) -> impl ExactSizeIterator<Item = T> {
        self.array.into_iter().skip(S).take(N - E - S)
    }

    /// Shorten the array by P at the start and Q at the end.
    ///
    /// The method return the same array but with a reduced set of visible elements.
    /// The returned array has a type that differ only by the generic constants S and E.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([3, 8, 4, 2, 6, 1]);
    /// assert_eq!(array.len(), 6);
    /// let sub_array = array.slice::<2, 2>();
    /// assert_eq!(sub_array.len(), 2);
    ///
    /// let test_array: [_; _] = sub_array.into();
    /// let ref_array = [4, 2];
    /// assert_eq!(test_array, ref_array);
    /// ```
    ///
    /// This method is only available for P and Q values that result in non negative array size.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([2, 3, 8, 5]).slice::<3, 2>();
    /// ```
    pub fn slice<const P: usize, const Q: usize>(self) -> Array<T, N, { S + P }, { E + Q }>
    where
        Assert<{ N >= (S + P) + (E + Q) }>: IsTrue,
    {
        unsafe {
            // This should be safe because the difference in generic constants S and E does not
            // affect the underlying array size
            transmute_unchecked::<Array<T, N, S, E>, Array<T, N, { S + P }, { E + Q }>>(self)
        }
    }

    /// Skip the P first elements of an array.
    ///
    /// The method return the same array but with a reduced set of visible elements.
    /// The returned array has a type that differ only by the generic constant S.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([3, 8, 4, 2, 6, 1]);
    /// assert_eq!(array.len(), 6);
    /// let sub_array = array.skip::<2>();
    /// assert_eq!(sub_array.len(), 4);
    ///
    /// let test_array: [_; _] = sub_array.into();
    /// let ref_array = [4, 2, 6, 1];
    /// assert_eq!(test_array, ref_array);
    /// ```
    ///
    /// This method is only available for a P value that result in non negative array size.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([2, 3, 8, 5]).skip::<5>();
    /// ```
    pub fn skip<const P: usize>(self) -> Array<T, N, { S + P }, { E + 0 }>
    where
        Assert<{ N >= (S + P) + (E + 0) }>: IsTrue,
    {
        self.slice::<P, 0>()
    }

    /// Skip the first element of an array.
    ///
    /// The method return the same array but with a reduced set of visible elements.
    /// The returned array has a type that differ only by the generic constant S.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([3, 8, 4, 2, 6, 1]);
    /// assert_eq!(array.len(), 6);
    /// let sub_array = array.skip_one();
    /// assert_eq!(sub_array.len(), 5);
    ///
    /// let test_array: [_; _] = sub_array.into();
    /// let ref_array = [8, 4, 2, 6, 1];
    /// assert_eq!(test_array, ref_array);
    /// ```
    ///
    /// This method is only available on an array of non null size.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).skip_one();
    /// ```
    pub fn skip_one(self) -> Array<T, N, { S + 1 }, { E + 0 }>
    where
        Assert<{ N >= (S + 1) + (E + 0) }>: IsTrue,
    {
        self.slice::<1, 0>()
    }

    /// Take the P first elements of an array.
    ///
    /// The method return the same array but with a reduced set of visible elements.
    /// The returned array has a type that differ only by the generic constant E.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([3, 8, 4, 2, 6, 1]);
    /// assert_eq!(array.len(), 6);
    /// let sub_array = array.take::<2>();
    /// assert_eq!(sub_array.len(), 2);
    ///
    /// let test_array: [_; _] = sub_array.into();
    /// let ref_array = [3, 8];
    /// assert_eq!(test_array, ref_array);
    /// ```
    ///
    /// This method is only available for a P value that result in non negative array size.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([2, 3, 8, 5]).take::<5>();
    /// ```
    pub fn take<const P: usize>(self) -> Array<T, N, { S + 0 }, { E + (N - E - S - P) }>
    where
        Assert<{ S + P < N - E }>: IsTrue,
        Assert<{ N >= (S + 0) + (E + (N - E - S - P)) }>: IsTrue,
    {
        self.slice::<0, { N - E - S - P }>()
    }

    /// Skip the Q last elements of an array.
    ///
    /// The method return the same array but with a reduced set of visible elements.
    /// The returned array has a type that differ only by the generic constant E.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([3, 8, 4, 2, 6, 1]);
    /// assert_eq!(array.len(), 6);
    /// let sub_array = array.skip_last::<2>();
    /// assert_eq!(sub_array.len(), 4);
    ///
    /// let test_array: [_; _] = sub_array.into();
    /// let ref_array = [3, 8, 4, 2];
    /// assert_eq!(test_array, ref_array);
    /// ```
    ///
    /// This method is only available for a P value that result in non negative array size.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([2, 3, 8, 5]).skip_last::<5>();
    /// ```
    pub fn skip_last<const Q: usize>(self) -> Array<T, N, { S + 0 }, { E + Q }>
    where
        Assert<{ N >= (S + 0) + (E + Q) }>: IsTrue,
    {
        self.slice::<0, Q>()
    }

    /// Skip the last element of an array.
    ///
    /// The method return the same array but with a reduced set of visible elements.
    /// The returned array has a type that differ only by the generic constant E.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([3, 8, 4, 2, 6, 1]);
    /// assert_eq!(array.len(), 6);
    /// let sub_array = array.skip_last_one();
    /// assert_eq!(sub_array.len(), 5);
    ///
    /// let test_array: [_; _] = sub_array.into();
    /// let ref_array = [3, 8, 4, 2, 6];
    /// assert_eq!(test_array, ref_array);
    /// ```
    ///
    /// This method is only available on an array of non null size.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).skip_last_one();
    /// ```
    pub fn skip_last_one(self) -> Array<T, N, { S + 0 }, { E + 1 }>
    where
        Assert<{ N >= (S + 0) + (E + 1) }>: IsTrue,
    {
        self.skip_last::<1>()
    }

    /// Folds every element into an accumulator by applying an operation,
    /// returning the final result.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// // Multiply elements of a non empty array
    /// assert_eq!(Array::from([2, 3, 8, 5]).fold(1, |x, y| x * y), 240);
    ///
    /// // Multiply elements of an empty array
    /// assert_eq!(Array::from([]).fold(1, |x, y| x * y), 1);
    ///
    /// // Sum elements of a non empty array
    /// assert_eq!(Array::from([2, 3, 8, 5]).fold(0, |x, y| x + y), 18);
    ///
    /// // Sum elements of an empty array
    /// assert_eq!(Array::from([]).fold(0, |x, y| x + y), 0);
    /// ```
    pub fn fold<B, F>(&self, init: B, mut f: F) -> B
    where
        Self: Sized,
        F: FnMut(B, &T) -> B,
    {
        let mut accum = init;
        for x in &self.array[S..(N - E)] {
            accum = f(accum, x);
        }
        accum
    }
}

/// Implementation of `Array` for all array sizes, with `Clone` bound on element type.
impl<T: Clone, const N: usize, const S: usize, const E: usize> Array<T, N, S, E>
where
    Assert<{ N >= S + E }>: IsTrue,
{
    /// Consolidates an array slice into an array of the actual slice size.
    ///
    /// The array's visible elements are clones into the new array.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    /// #![feature(generic_arg_infer)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// let array = Array::from([3, 8, 4, 2, 6, 1]);
    /// let slice = array.slice::<1, 3>();
    /// let array = slice.consolidate();
    /// assert_eq!(Into::<[i32; _]>::into(array), [8, 4]);
    /// ```
    pub fn consolidate(&self) -> Array<T, { N - S - E }> {
        let mut array = MaybeUninit::<T>::uninit_array::<{ N - S - E }>();
        for (index, value) in self.iter().cloned().enumerate() {
            array[index].write(value);
        }
        Array::from(unsafe { MaybeUninit::array_assume_init(array) })
    }
}

/// Implementation of `Array` for non empty arrays.
impl<T, const N: usize, const S: usize, const E: usize> Array<T, N, S, E>
where
    Assert<{ N > S + E }>: IsTrue,
{
    /// Return the first element of the array.
    ///
    /// This method is only available on an array of non null size.
    /// The following example fails to compile.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).array_first();
    /// ```
    ///
    /// Here is an example of the method used on an array of non null size.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// assert_eq!(Array::from([4, 5, 7, 2, 4, 5]).array_first(), &4);
    /// ```
    pub fn array_first(&self) -> &T {
        &self.array[S]
    }

    /// Return the last element of the array.
    ///
    /// This method is only available on an array of non null size.
    /// The following example fails to compile.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).array_last();
    /// ```
    ///
    /// Here is an example of the method used on an array of non null size.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// assert_eq!(Array::from([4, 5, 7, 2, 4, 5]).array_last(), &5);
    /// ```
    pub fn array_last(&self) -> &T {
        &self.array[N - E - 1]
    }

    /// Return the nth element of the array.
    ///
    /// This method is only available on an array of non null size.
    /// The following example fails to compile.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).array_nth::<0>();
    /// ```
    ///
    /// Here is an example of the method used on an array of non null size.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// assert_eq!(Array::from([4, 5, 7, 2, 4, 5]).array_nth::<2>(), &7);
    /// ```
    ///
    /// The method also ensures that the value being accessed exists.
    /// The following example fails to compile.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([4, 5, 7, 2, 4, 5]).array_nth::<7>();
    /// ```
    pub fn array_nth<const K: usize>(&self) -> &T
    where
        Assert<{ S + K < N - E }>: IsTrue,
    {
        &self.array[S + K]
    }
}

/// Implementation of `Array` for non empty arrays, with `Ord` bound on element type.
impl<T: Ord, const N: usize, const S: usize, const E: usize> Array<T, N, S, E>
where
    Assert<{ N > S + E }>: IsTrue,
{
    /// Get array maximum value.
    ///
    /// If several values are equal to the maximum, return the first one.
    ///
    /// This method is only available on an array of non null size.
    /// The following example fails to compile.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).array_max();
    /// ```
    ///
    /// Here is an example of the method used on an array of non null size.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// assert_eq!(Array::from([4, 5, 7, 2, 4, 5]).array_max(), &7);
    /// ```
    pub fn array_max(&self) -> &T {
        let mut max_value = &self.array[S];
        for x in (S + 1)..(N - E) {
            let test_value = &self.array[x];
            if test_value > max_value {
                max_value = test_value;
            }
        }
        max_value
    }

    /// Get array minimum value.
    ///
    /// If several values are equal to the minimum, return the first one.
    ///
    /// This method is only available on an array of non null size.
    /// The following example fails to compile.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).array_min();
    /// ```
    ///
    /// Here is an example of the method used on an array of non null size.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// assert_eq!(Array::from([4, 5, 7, 2, 4, 5]).array_min(), &2);
    /// ```
    pub fn array_min(&self) -> &T {
        let mut max_value = &self.array[S];
        for x in (S + 1)..(N - E) {
            let test_value = &self.array[x];
            if test_value < max_value {
                max_value = test_value;
            }
        }
        max_value
    }
}

/// Implementation of `Array` for non empty arrays, with `Clone` bound on element type.
impl<T: Clone, const N: usize, const S: usize, const E: usize> Array<T, N, S, E>
where
    Assert<{ N > S + E }>: IsTrue,
{
    /// Reduces the elements to a single one, by repeatedly applying a reducing
    /// operation.
    ///
    /// This method is only available on an array of non null size.
    /// The following example fails to compile.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).reduce(|x, y| x + y);
    /// ```
    ///
    /// Here is an example of the method used on an array of non null size.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// assert_eq!(Array::from([4, 5, 7, 2, 4, 5]).reduce(|x, y| x + y), 27);
    /// ```
    pub fn reduce<F>(&self, mut f: F) -> T
    where
        F: FnMut(&T, &T) -> T,
    {
        let mut accum = self.array[S].clone();
        for x in &self.array[(S + 1)..(N - E)] {
            accum = f(&accum, x);
        }
        accum
    }
}

/// Implementation of `Array` for non empty arrays, with `Clone` and `Add` bounds on element type.
impl<T: Clone, const N: usize, const S: usize, const E: usize> Array<T, N, S, E>
where
    for<'a> &'a T: Add<&'a T, Output = T>,
    Assert<{ N > S + E }>: IsTrue,
{
    /// Sums the elements of the array.
    ///
    /// This method is only available on an array of non null size.
    /// The following example fails to compile.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).sum();
    /// ```
    ///
    /// Here is an example of the method used on an array of non null size.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// assert_eq!(Array::from([4, 5, 7, 2, 4, 5]).sum(), 27);
    /// ```
    pub fn sum(&self) -> T {
        self.reduce(|x, y| x + y)
    }
}

/// Implementation of `Array` for non empty arrays, with `Clone` and `Mul` bounds on element type.
impl<T: Clone, const N: usize, const S: usize, const E: usize> Array<T, N, S, E>
where
    for<'a> &'a T: Mul<&'a T, Output = T>,
    Assert<{ N > S + E }>: IsTrue,
{
    /// Multiply the elements of the array.
    ///
    /// This method is only available on an array of non null size.
    /// The following example fails to compile.
    ///
    /// ```compile_fail
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// Array::from([]).product();
    /// ```
    ///
    /// Here is an example of the method used on an array of non null size.
    ///
    /// ```
    /// #![allow(incomplete_features)]
    /// #![feature(generic_const_exprs)]
    ///
    /// use rust_suggestions::array::Array;
    ///
    /// assert_eq!(Array::from([4, 5, 7, 2, 4, 5]).product(), 5600);
    /// ```
    pub fn product(&self) -> T {
        self.reduce(|x, y| x * y)
    }
}

#[cfg(test)]
mod test {
    use super::Array;
    use core::mem::transmute;

    #[test]
    fn test_max() {
        assert_eq!(Array::from([3]).array_max(), &3);
        assert_eq!(Array::from([3, 3, 4, 2, 6, 3]).array_max(), &6);
    }

    #[test]
    fn test_min() {
        assert_eq!(Array::from([3]).array_min(), &3);
        assert_eq!(Array::from([3, 3, 4, 2, 6, 3]).array_min(), &2);
    }

    #[test]
    fn test_sum() {
        assert_eq!(Array::from([3]).sum(), 3);
        assert_eq!(Array::from([3, 3, 4, 2, 6, 3]).sum(), 21);
    }

    #[test]
    fn test_product() {
        assert_eq!(Array::from([3]).product(), 3);
        assert_eq!(Array::from([3, 3, 4, 2, 6, 3]).product(), 1296);
    }

    #[test]
    fn test_first() {
        assert_eq!(Array::from([3]).array_first(), &3);
        assert_eq!(Array::from([3, 1, 4, 2, 6, 1]).array_first(), &3);
    }

    #[test]
    fn test_last() {
        assert_eq!(Array::from([3]).array_last(), &3);
        assert_eq!(Array::from([3, 1, 4, 2, 6, 1]).array_last(), &1);
    }

    #[test]
    fn test_nth() {
        assert_eq!(Array::from([3]).array_nth::<0>(), &3);
        assert_eq!(Array::from([3, 1, 4, 2, 6, 1]).array_nth::<3>(), &2);
    }

    #[test]
    fn test_slice() {
        let array = Array::from([3, 8, 4, 2, 6, 1]);
        assert_eq!(array.len(), 6);
        let res = array.slice::<3, 2>();
        assert_eq!(res.len(), 1);
        assert_eq!(res.array_max(), &2);
        assert_eq!(res.array_min(), &2);
        assert_eq!(res.array_first(), &2);
        assert_eq!(res.array_last(), &2);
    }

    #[test]
    fn test_skip() {
        let array = Array::from([3, 8, 4, 2, 6, 1]);
        assert_eq!(array.len(), 6);
        let res = array.skip::<2>();
        assert_eq!(res.len(), 4);
        assert_eq!(res.array_max(), &6);
        assert_eq!(res.array_min(), &1);
        assert_eq!(res.array_first(), &4);
        assert_eq!(res.array_last(), &1);
    }

    #[test]
    fn test_skip_one() {
        let array = Array::from([3, 8, 4, 2, 6, 1]);
        let res = array.skip_one();
        assert_eq!(res.array_first(), &8);
        let res = res.skip_one();
        assert_eq!(res.array_first(), &4);
    }

    #[test]
    fn test_take() {
        let array = Array::from([3, 8, 4, 2, 6, 1]);
        assert_eq!(array.len(), 6);
        let res = array.take::<2>();
        assert_eq!(res.len(), 2);
        assert_eq!(res.array_max(), &8);
        assert_eq!(res.array_min(), &3);
        assert_eq!(res.array_first(), &3);
        assert_eq!(res.array_last(), &8);
    }

    #[test]
    fn test_skip_last() {
        let array = Array::from([3, 8, 4, 2, 6, 1]);
        assert_eq!(array.len(), 6);
        let res = array.skip_last::<2>();
        assert_eq!(res.len(), 4);
        assert_eq!(res.array_max(), &8);
        assert_eq!(res.array_min(), &2);
        assert_eq!(res.array_first(), &3);
        assert_eq!(res.array_last(), &2);
    }

    #[test]
    fn test_skip_last_one() {
        let array = Array::from([3, 8, 4, 2, 6, 1]);
        let res = array.skip_last_one();
        assert_eq!(res.array_last(), &6);
        let res = res.skip_last_one();
        assert_eq!(res.array_last(), &2);
    }

    #[test]
    fn test_iter() {
        let array = Array::from([3, 8, 4, 2, 6, 1]);
        let slice = array.slice::<2, 2>();
        let ref_array = [4, 2];
        let mut count = 0;
        for (index, value) in slice.iter().enumerate() {
            assert_eq!(value, &ref_array[index]);
            count += 1;
        }
        assert_eq!(ref_array.len(), count);
    }

    #[test]
    fn test_into_iter() {
        let array = Array::from([3, 8, 4, 2, 6, 1]);
        let slice = array.slice::<2, 2>();
        let ref_array = [4, 2];
        let mut count = 0;
        for (index, value) in slice.into_iter().enumerate() {
            assert_eq!(value, ref_array[index]);
            count += 1;
        }
        assert_eq!(ref_array.len(), count);
    }

    #[test]
    fn test_into() {
        let array = Array::from([3, 8, 4, 2, 6, 1]);
        let slice = array.slice::<2, 2>();
        let test_array: [_; _] = slice.into();
        let ref_array = [4, 2];
        assert_eq!(test_array, ref_array);
    }

    #[test]
    fn test_from_into() {
        let array_src = [1, 2, 3, 4];
        let array_from = Array::from(array_src.clone());
        let array_to: [_; _] = array_from.into();
        assert_eq!(array_to, array_src);
    }

    #[test]
    fn test_transmute() {
        let array_src = [1, 2, 3, 4];
        let array_from = unsafe { transmute::<_, Array<i32, 4>>(array_src.clone()) };
        assert_eq!(array_from.array_nth::<2>(), &3);
        assert_eq!(array_from.len(), 4);
        let array_to = unsafe { transmute::<_, [i32; _]>(array_from) };
        assert_eq!(array_to, array_src);
    }
}
