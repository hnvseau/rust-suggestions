//! A module allowing to check const generic conditions

/// Assertion enum used to validate conditions on generic const
///
/// In the following example, the generic constant `N` must be greater that `1`.
/// The trait `IsTrue` is only implemented for `Assert<true>` ensuring the condition `{N > 1}` is
/// `true`.
///
/// ```
/// #![allow(incomplete_features)]
/// #![feature(generic_const_exprs)]
///
/// use rust_suggestions::const_expr_utils::{Assert, IsTrue};
///
/// pub struct NonEmptyArray<const N: usize>
/// where
///     Assert<{N > 0}>: IsTrue
/// {
///     array: [u8; N],
/// }
/// ```
pub enum Assert<const CHECK: bool> {}

/// Marker trait only implemented for `Assert<true>`
pub trait IsTrue {}

impl IsTrue for Assert<true> {}
