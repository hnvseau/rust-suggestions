//! # Rust suggestions
//!
//! This library is about testing things with Rust, that may or may not lead to evolutions of the
//! language.
//!
//! The `array` module is about adding methods to built-in arrays, to leverage their known size at
//! compile time.
//!
//! The `const_expr_utils` module provides a way to constrain generic const expressions.
//! This solution is from [this book](https://practice.course.rs/generics-traits/const-generics.html)
//! and is used by the other modules.

// Language should rely on core only
#![no_std]
// Ennable generic const nightly function
#![allow(incomplete_features)]
#![feature(generic_const_exprs)]
#![feature(generic_arg_infer)]
// Enable internale features
#![allow(internal_features)]
#![feature(core_intrinsics)]
// Enable not initialized arrays
#![feature(maybe_uninit_uninit_array)]
#![feature(maybe_uninit_array_assume_init)]
// Warn about undocumented public items
#![warn(missing_docs)]

pub mod array;
pub mod const_expr_utils;
