# Rust Suggestions

This library is about testing things with Rust, that may or may not lead to evolutions of the
language.

The `array` module is about adding methods to built-in arrays, to leverage their known size at
compile time.

The `const_expr_utils` module provides a way to constrain generic const expressions.
This solution is from [this book](https://practice.course.rs/generics-traits/const-generics.html)
and is used by the other modules.

## Documentation

A more complete description of each module is available in the documentation.
Run:

```sh
cargo doc --open
```

## Tests

Tests from the modules and the module's documentation can be tested using:

```sh
cargo test --release
```
